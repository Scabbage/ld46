using System;
using Ant;
using AntCamera;
using UnityEngine;

namespace UI
{
	public class SelectionManager : MonoBehaviour
	{
		private CameraController _cameraController;

		private void OnEnable()
		{
			_camera = Camera.main;
			_cameraController = _camera.GetComponent<CameraController>();
		}

		public delegate void AntSelectHandler(Selectable ant);

		public event AntSelectHandler OnAntSelected;

		public void SelectAnt(Selectable ant)
		{
			_cameraController.FocusAnt(ant);
			OnAntSelected?.Invoke(ant);
		}

		private Camera _camera;

		private void Update()
		{
			if (Input.GetMouseButtonDown(0))
			{
				var mouseWorldPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
				RaycastHit2D hit = Physics2D.Raycast(mouseWorldPosition, Vector2.zero);
				Selectable ant = null;
				if (hit.collider != null)
				{
					ant = hit.collider.GetComponent<Selectable>();
				}

				SelectAnt(ant);
			}

			if (Input.GetMouseButtonDown(1))
			{
				SelectAnt(null);
			}
		}
	}
}