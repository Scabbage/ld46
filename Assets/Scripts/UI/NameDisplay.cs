﻿using System;
using Ant;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class NameDisplay : MonoBehaviour
    {
        public Text nameString;
        private AntInfoPanel _parentPanel;
        private Selectable _ant => _parentPanel.selectedAnt;

        private void OnEnable()
        {
            _parentPanel = GetComponentInParent<AntInfoPanel>();
            
        }

        private void Update()
        {
            nameString.text = _ant.nameString;
        }
    }
}