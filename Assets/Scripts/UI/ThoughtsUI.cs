using System;
using System.Text;
using Ant;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	public class ThoughtsUI : MonoBehaviour
	{
		public Text uiThoughts;
		private SelectionManager _selectionManager;
		private AntInfoPanel _parentPanel;
		public ThoughtLog _thoughtLog;
		private StringBuilder _stringBuilder = new StringBuilder();

		private void Awake()
		{
			_selectionManager = GetComponentInParent<SelectionManager>();
			_selectionManager.OnAntSelected += antSelected;
			_parentPanel = GetComponentInParent<AntInfoPanel>();
		}

		private void antSelected(Selectable ant)
		{
			if (_thoughtLog != null)
			{
				_thoughtLog.OnThoughtsChanged -= updateThoughts;
			}

			if (ant != null)
			{
				_thoughtLog = ant.thoughtLog;
				_thoughtLog.OnThoughtsChanged += updateThoughts;
				ant.thoughtLog.Invalidate();
			}
			else
			{
				_thoughtLog = null;
			}
		}

		private void updateThoughts(string[] log)
		{
			_stringBuilder.Clear();
			foreach (var entry in log)
			{
				_stringBuilder.AppendFormat("{0}\n", entry);
			}

			uiThoughts.text = _stringBuilder.ToString();
		}
	}
}