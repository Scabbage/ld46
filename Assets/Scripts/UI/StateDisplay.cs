using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	public class StateDisplay : MonoBehaviour
	{
		public Text uiAntState;
		private AntInfoPanel _parentPanel;
		private Selectable _ant => _parentPanel.selectedAnt;

		private void OnEnable()
		{
			_parentPanel = GetComponentInParent<AntInfoPanel>();
		}

		private void Update()
		{
			if (_ant.State == null) return;

			uiAntState.text = _ant.State.Name;
		}
	}
}