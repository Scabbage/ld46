using Ant;
using AntCamera;
using UnityEngine;

namespace UI
{
	public class AntInfoPanel : MonoBehaviour
	{
		public Vector2 offset = new Vector2(50, 20);
		private RectTransform _transform;
		private Camera _camera;
		public Selectable selectedAnt;
		private SelectionManager _selectionManager;
		private CameraController _cameraController;

		private void OnEnable()
		{
			_transform = GetComponent<RectTransform>();
			_selectionManager = GetComponentInParent<SelectionManager>();
			_selectionManager.OnAntSelected += AntSelected;
			_camera = Camera.main;
			_cameraController = _camera.GetComponent<CameraController>();
			gameObject.SetActive(selectedAnt != null);
		}

		private void AntSelected(Selectable ant)
		{
			selectedAnt = ant;

			if (selectedAnt != null)
			{
				gameObject.SetActive(true);
				SetPanelWorldPosition(selectedAnt.Position);
			}
			else
			{
				gameObject.SetActive(false);
			}
		}

		private void Update()
		{
			SetPanelWorldPosition(selectedAnt.Position + offset);
		}

		private void SetPanelWorldPosition(Vector2 position)
		{
			_transform.anchoredPosition = _camera.WorldToScreenPoint(position);
		}
	}
}