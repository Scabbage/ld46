﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UILoader : MonoBehaviour
{
	private void OnEnable()
	{
		SceneManager.LoadScene("Scenes/UI", LoadSceneMode.Additive);
	}
}