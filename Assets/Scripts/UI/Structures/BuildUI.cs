﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildUI : MonoBehaviour, IPointerDownHandler
{
    public GameObject structurePrefab;


    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Clicked structure builder");
        Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        position.z = -1f;
        Instantiate(structurePrefab, position, Quaternion.identity);
    }

}
