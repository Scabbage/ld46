﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RectStructure : Structure
{
    public Vector2 customSize;

    public override Vector2 Bounds => customSize;
}
