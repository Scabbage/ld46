﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public abstract class Structure : MonoBehaviour
{
    bool CanPlace
    {
        get
        {
            return World.Instance.map.CanAddChamber(transform.position, Bounds);
        }
    }

    public abstract Vector2 Bounds { get; }

    Material mat;
    Color32 validColor, invalidColor;

    bool couldPlaceLastFrame;

    Transform connectionPreview;

    ColonyMap map;

    private void Start()
    {
        mat = GetComponent<Renderer>().material;
        transform.localScale = Bounds * 2f;
        validColor = Color.green;
        validColor.a = 50;
        invalidColor = Color.red;
        invalidColor.a = 50;


        connectionPreview = GameObject.CreatePrimitive(PrimitiveType.Quad).transform;
        connectionPreview.GetComponent<Renderer>().material = mat;

        map = World.Instance.map;
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            if (CanPlace)
                map.AddChamber(transform.position, Bounds);

            Destroy(gameObject);
            Destroy(connectionPreview.gameObject);
        }

        UpdateConnectionPreview();

        if (CanPlace)
        {

            if (!couldPlaceLastFrame)
                mat.color = validColor;

            couldPlaceLastFrame = true;
        }else
        {
            if (couldPlaceLastFrame)
                mat.color = invalidColor;

            couldPlaceLastFrame = false;
        }

        Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        position.z = -1.5f;

        transform.position = position;


    }

    void UpdateConnectionPreview()
    {
        Vector2 left, right;
        left = (Vector2)transform.position - Bounds;
        right = left;
        right.x += Bounds.x * 2f;

        Vector2 cp1, cp2;
        cp1 = map.ClosestPointInColony(left);
        cp2 = map.ClosestPointInColony(right);

        if (Vector2.Distance(cp1, left) < Vector2.Distance(cp2, right))
        {
            SetConnectionPreview(cp1, left);
        }
        else
        {
            SetConnectionPreview(cp2, right);
        }
    }

    void SetConnectionPreview(Vector2 start, Vector2 end)
    {
        float dist = Vector2.Distance(start, end);

        float thickness =  (map.tunnelWidth + 10f) / (dist + 10f);
        
        connectionPreview.localScale = new Vector3(dist, thickness, 1f);
        connectionPreview.position = (start + end) / 2f;
        connectionPreview.rotation = Quaternion.FromToRotation(Vector3.right, end - start);


    }

}
