﻿using UnityEngine;

namespace Queen
{
    public class QueenStruggleState : BaseQueenState
    {
        public QueenStruggleState(Queen queen) : base(queen)
        { }

        public override string Name => "Struggling";

        private float energyCostPerSecond = 5f;

        private float timeSinceIdleThoughts;

        public override void OnStateEnter()
        {
            //_queen.thoughtLog.AddThought(Ant.Phrases.QueenStruggling);
        }

        public override void Tick(float deltaTime)
        {
            timeSinceIdleThoughts += deltaTime;
            if (timeSinceIdleThoughts > Random.Range(5f, 15f))
            {
                _queen.thoughtLog.AddThought(Ant.Phrases.QueenStruggling);
                timeSinceIdleThoughts = 0f;
            }
            

            _queen.energy.AddEnergy(-(energyCostPerSecond * deltaTime));
        }
    }
}