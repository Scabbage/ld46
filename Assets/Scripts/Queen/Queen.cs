using Ant;
using UnityEngine;

namespace Queen
{
	public class Queen : MonoBehaviour
	{
		private BaseQueenState _state;
		public BaseQueenState State
		{
			get => _state;
			set
			{
				_state?.OnStateExit();
				_state = value;
				selectable.State = _state;
				_state?.OnStateEnter();
			}
		}

        public static Queen main;
		public AntController AntPrefab;
		public float dayLengthInSeconds = 300f;
		public float age = 0;
		public Selectable selectable;
		public ThoughtLog thoughtLog;

        public Energy energy;

        private bool isDrowning = false;
        private bool _isAliveInit = true;

		private void OnEnable()
		{
            main = this;
			selectable = GetComponent<Selectable>();
			thoughtLog = GetComponent<ThoughtLog>();
            energy = GetComponent<Energy>();
			State = new SpawningState(this);
            selectable.nameString = "Queen";
		}

		private void Update()
		{
			age += Time.deltaTime;
			_state?.Tick(Time.deltaTime);

            if (_isAliveInit)
            {
                selectable.ageInDays = Mathf.FloorToInt(age / dayLengthInSeconds);
            }

            //MAKE ANTS DROWN IN WATER
            Vector3 thtransform = transform.position;
            int intx = (int)thtransform.x;
            int inty = (int)thtransform.y;
            int wetnessInt = World.Instance.voxelmap.Wetness(intx, inty);
            if (wetnessInt >= 15 && !isDrowning && _isAliveInit)
            {
                State = new QueenStruggleState(this);
                isDrowning = true;
            }
            if (wetnessInt <= 14 && isDrowning && _isAliveInit)
            {
                State = new IdleState(this);
                isDrowning = false;
            }
        }

        private void LateUpdate()
        {

            //THIS IS THE QUEEN DYING
            if (energy.energy <= 0 && _isAliveInit)
            {
                _isAliveInit = false;
                State = new QueenDeathState(this);
            }
        }
    }
}