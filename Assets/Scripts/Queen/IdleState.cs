using UnityEngine;

namespace Queen
{
    public class IdleState : BaseQueenState
    {
        public IdleState(Queen queen) : base(queen)
        { }

        public override string Name => "Idle";

        private float energyCostPerSecond = 0.05f;
        private bool isLowEnergy = true;

        private float timeSinceIdleThoughts;

        public override void Tick(float deltaTime)
        {
            //Ants talk to themselves when idling.
            timeSinceIdleThoughts += deltaTime;
            if (timeSinceIdleThoughts > Random.Range(5f, 15f))
            {
                _queen.thoughtLog.AddThought(Ant.Phrases.QueenIdle);
                timeSinceIdleThoughts = 0f;
            }

            _queen.energy.AddEnergy(-(energyCostPerSecond * deltaTime));
            if (_queen.energy.energy >= (.5f * _queen.energy.maxEnergy) && isLowEnergy)
            {
                isLowEnergy = false;
                _queen.State = new SpawningState(_queen);
            }
        }
    }
}