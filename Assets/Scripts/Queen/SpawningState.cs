using Ant;
using UnityEngine;

namespace Queen
{
	public class SpawningState : BaseQueenState
	{
		public float _secondsPerSpawn = 5;
		public int spawnLimit = 5;
		private int _spawnCount;
		private float _time;

        private float energyCostPerSecond = 0.15f;
        private bool isLowEnergy = false;


        public SpawningState(Queen queen) : base(queen)
		{ }

		public override string Name => "Birthin'";

		public override void Tick(float deltaTime)
		{
			base.Tick(deltaTime);

			_time += Time.deltaTime;
			if (_time >= _secondsPerSpawn)
			{
				_time -= _secondsPerSpawn;
				SpawnAnt();
				_queen.thoughtLog.AddThought(Phrases.QueenBirthing);
			}

			if (_spawnCount >= spawnLimit)
			{
				_queen.State = new IdleState(_queen);
			}

            _queen.energy.AddEnergy(-(energyCostPerSecond * deltaTime));
            if (_queen.energy.energy <= (.5f * _queen.energy.maxEnergy) && !isLowEnergy)
            {
                isLowEnergy = true;
                _queen.State = new IdleState(_queen);
            }
		}

		private void SpawnAnt()
		{
			var parent = _queen.transform;
			var position = (Vector2) parent.position + Random.insideUnitCircle;
			AntController.Instantiate(_queen.AntPrefab, position, Quaternion.identity, parent);
			_spawnCount++;
		}
	}
}