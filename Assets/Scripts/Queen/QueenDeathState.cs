﻿using UnityEngine;

namespace Queen
{
    public class QueenDeathState : BaseQueenState
    {
        public override string Name => "Dead";

        public QueenDeathState(Queen queen) : base(queen)
        {
            queen.GetComponent<Animator>().speed = 0;
            queen.GetComponent<SpriteRenderer>().flipY = true;

        }

        public override void OnStateEnter()
        {
            _queen.thoughtLog.AddThought(Ant.Phrases.QueenDeath);
        }

        public override void Tick(float deltaTime)
        {
        }
    }
}