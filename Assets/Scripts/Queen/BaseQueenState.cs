using UnityEngine;

namespace Queen
{
	public abstract class BaseQueenState : IState
	{
		protected Queen _queen;

		public BaseQueenState(Queen queen)
		{
			_queen = queen;
		}

		public abstract string Name { get; }

		public virtual void OnStateEnter()
		{ }

		public virtual void OnStateExit()
		{ }

		public virtual void Tick(float deltaTime)
		{
			_queen.selectable.ageInDays = Mathf.FloorToInt(_queen.age / 300f);
		}
	}
}