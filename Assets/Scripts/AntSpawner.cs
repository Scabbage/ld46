using System;
using Ant;
using UnityEngine;

namespace DefaultNamespace
{
	public class AntSpawner : MonoBehaviour
	{
		public AntController AntPrefab;
		public float _secondsPerSpawn = 5;
		public int spawnLimit = 15;
		private int _spawnCount;

		public void SpawnAnt()
		{
			AntController.Instantiate(AntPrefab, transform.position, Quaternion.identity, transform);
			_spawnCount++;
		}

		private float _time;

		private void Update()
		{
			if (_spawnCount >= spawnLimit) return;

			_time += Time.fixedDeltaTime;
			if (_time >= _secondsPerSpawn)
			{
				_time -= _secondsPerSpawn;
				SpawnAnt();
			}
		}
	}
}