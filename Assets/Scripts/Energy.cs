using UnityEngine;

public class Energy : MonoBehaviour
{
    public delegate void OutOfEnergyHandler();
    public event OutOfEnergyHandler OutOfEnergy;

    public float maxEnergy = 100;
    public float energy;

    private void Start()
    {
        energy = maxEnergy;
    }

    public void AddEnergy(float amount)
    {
        energy += amount;
        if (energy <= 0)
        {
            energy = 0;
            OutOfEnergy?.Invoke();
        }
        else if (energy > maxEnergy)
        {
            energy = maxEnergy;
        }
    }
}