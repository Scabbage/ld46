public interface IState
{
	string Name { get; }

	void OnStateEnter();

	void OnStateExit();

	void Tick(float deltaTime);
}