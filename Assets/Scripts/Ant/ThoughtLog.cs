using System;
using System.Collections.Generic;
using UnityEngine;

namespace Ant
{
	public class ThoughtLog : MonoBehaviour
	{
		public delegate void ThoughtsChangedHandled(string[] log);
		public event ThoughtsChangedHandled OnThoughtsChanged;
		
		private Queue<string> _thoughts = new Queue<string>(9);
		private int _thoughtCap = 9;

		public void AddThought(string thought)
		{
			_thoughts.Enqueue(thought);
			while (_thoughts.Count > _thoughtCap)
			{
				_thoughts.Dequeue();
			}
			OnThoughtsChanged?.Invoke(_thoughts.ToArray());
		}

		public void Invalidate()
		{
			OnThoughtsChanged?.Invoke(_thoughts.ToArray());
		}
	}
}