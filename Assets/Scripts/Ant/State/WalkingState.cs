using UnityEngine;

namespace Ant
{
    public class WalkingState : BaseAntState
    {
        public override string Name => "Walking";
        private float _moveSpeed = 0.1f;
        private float timeSpentWalking;
        private float energyCostPerSecond = 0.15f;

        public WalkingState(AntController ant, Task task) : base(ant)
        {
            _ant.CurrentTask = task;
            ant.GetComponent<Animator>().SetBool("Walking", true);
        }

        public override void OnStateEnter()
        {
            //_ant.thoughts.AddThought(Phrases.Walking);
            timeSpentWalking = 0;
        }


        public override void Tick(float deltaTime)
        {
            //Ant has thought if walking for long enough.
            timeSpentWalking += deltaTime;
            if (timeSpentWalking > Random.Range(2f, 6f))
            {
                _ant.thoughts.AddThought(Phrases.Walking);
                //Debug.Log("talked while walking");
                timeSpentWalking = 0f;
            }

            if(Random.Range(0f, 1f) < 0.1f)
                FindNewTask();

            if (_ant.MoveTowards(_ant.CurrentTask.position))
            {
                _ant.State = new WorkingState(_ant, _ant.CurrentTask);
            }

            _ant._energy.AddEnergy(-(energyCostPerSecond * deltaTime));
        }
    }
}