﻿using UnityEngine;

namespace Ant
{
    public class StruggleState : BaseAntState
    {
        public override string Name => "Struggling";

        private float energyCostPerSecond = 5f;

        private float timeSinceIdleThoughts;

        private bool floatingUpwards = false;

        private Vector2 _struggleDestination;

        public StruggleState(AntController ant) : base(ant)
        {
            ant.GetComponent<Animator>().SetBool("Walking", false);
        }

        public override void OnStateEnter()
        {
            _ant.thoughts.AddThought(Phrases.Struggling);
            floatingUpwards = false;
        }

        public override void Tick(float deltaTime)
        {

            _ant._energy.AddEnergy(-(energyCostPerSecond * deltaTime));

            timeSinceIdleThoughts += deltaTime;
            if (timeSinceIdleThoughts > Random.Range(3f, 9f))
            {
                _ant.thoughts.AddThought(Phrases.Struggling);

                //FloatUpwards();

                timeSinceIdleThoughts = 0f;
            }



            //FloatUpwards();

        }

        void FloatUpwards()
        {
            Vector2 pos = _transform.position;
            Vector2 posUp = pos;
            posUp.y += 99f;
            if (!floatingUpwards)
            {

                _transform.Translate(Vector3.up * Time.deltaTime, Space.World);
                floatingUpwards = true;
            }
            
            
        }

        
        void FindNewDestination()
        {
            if (World.Instance == null)
            {
                _struggleDestination = _transform.position;
            }
            else
            {
                
                var pos = _transform.position;

                Vector2Int nodePos = new Vector2Int(Mathf.CeilToInt(pos.x), Mathf.CeilToInt(pos.y));

                var lastPos = nodePos;
                
                        lastPos = nodePos;
                        nodePos.y++;
                    
                _struggleDestination = lastPos;
            }
        }
    }
    
}