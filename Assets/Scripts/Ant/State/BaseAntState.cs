using UnityEngine;

namespace Ant
{
	public abstract class BaseAntState : IState
	{
		public abstract string Name { get; }
		protected AntController _ant;
		protected Transform _transform;

		public BaseAntState(AntController ant)
		{
			_ant = ant;
			_transform = ant.GetComponent<Transform>();
		}

        protected void FindNewTask()
        {
            var closestTask = _ant.ClosestTask();
            if (closestTask != null)
            {
                if(_ant.State is WalkingState walk)
                    _ant.thoughts.AddThought(Phrases.Walking);

                _ant.State = new WalkingState(_ant, closestTask);
            }
            else
            {
                _ant.State = new IdleState(_ant);
                //Debug.Log("No tasks nearby, defaulting to idle.");
            }
        }

        public virtual void OnStateEnter()
		{ }

		public virtual void OnStateExit()
		{ }

		public virtual void Tick(float deltaTime)
		{ }
	}
}