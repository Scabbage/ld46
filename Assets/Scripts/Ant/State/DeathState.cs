﻿using UnityEngine;

namespace Ant
{
    public class DeathState : BaseAntState
    {
        public override string Name => "Dead";

        public DeathState(AntController ant) : base(ant)
        {
            ant.GetComponent<Animator>().SetBool("Die", true);
        }

        public override void OnStateEnter()
        {
            _ant.thoughts.AddThought(Phrases.Death);
        }

        public override void Tick(float deltaTime)
        {
        }
    }
}
