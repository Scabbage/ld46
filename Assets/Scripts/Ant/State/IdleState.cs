using System.Collections.Generic;
using UnityEngine;

namespace Ant
{
    public class IdleState : BaseAntState
    {
        public override string Name => "Idle";

        private float _time;
        public float secondsPerUpdate = 1;

        private float timeSinceIdleThoughts;

        private float energyCostPerSecond = 0.05f;

        private bool isGrounded = false;
        private bool isStandingStill = false;

        private Vector2 _idleDestination;

        Animator anim;

        public IdleState(AntController ant) : base(ant)
        {
            FindNewDestination();
            anim = ant.GetComponent<Animator>();
            anim.SetBool("Walking", false);
        }

        public override void OnStateEnter()
        {
            //_ant.thoughts.AddThought(Phrases.Idle);
            isStandingStill = true;
            isGrounded = true;
        }

        public override void Tick(float deltaTime)
        {
            _time += deltaTime;
            if (_time > secondsPerUpdate)
            {
                _time -= secondsPerUpdate;
                var closestTask = _ant.ClosestTask();
                if (closestTask != null)
                {
                    _ant.State = new WalkingState(_ant, closestTask);
                }
            }
            //Ants talk to themselves when they get bored.
            timeSinceIdleThoughts += deltaTime;
            if (timeSinceIdleThoughts > Random.Range(5f, 15f))
            {
                _ant.thoughts.AddThought(Phrases.Idle);

                //ANTS WANDER A BIT WHEN THEY GET BORED
                var pos = _transform.position;
                pos.x += Random.Range(-25f, 25f);
                Vector2Int nodePos = new Vector2Int(Mathf.CeilToInt(pos.x), Mathf.CeilToInt(pos.y));
                if (World.Instance.voxelmap.IsAir(nodePos))
                {
                    _ant.MoveTowards(pos);
                    _idleDestination = pos;

                }
                else
                {
                    pos.y += Random.Range(-5f, 5f);
                    nodePos = new Vector2Int(Mathf.CeilToInt(pos.x), Mathf.CeilToInt(pos.y));
                    if (World.Instance.voxelmap.IsAir(nodePos))
                    {
                        _ant.MoveTowards(pos);
                        _idleDestination = pos;
                    }
                    
                }

                timeSinceIdleThoughts = 0f;
            }

            

                _ant._energy.AddEnergy(-(energyCostPerSecond * deltaTime));

            Move();
        }

        private void Move()
        {
            if (_ant.MoveTowards(_idleDestination))
            {
                FindNewDestination();

                anim.SetBool("Walking", false);

            }
            else
            {
                anim.SetBool("Walking", true);
                timeSinceIdleThoughts = 0f;
            }
        }

        void FindNewDestination()
        {
            if (World.Instance == null)
            {
                _idleDestination = _transform.position;
            }
            else
            {
                /*
                float searchRadius = 1f;
                while (World.Instance.voxelmap.IsAir(
                    new Vector2Int(
                        Mathf.RoundToInt(_idleDestination.x),
                        Mathf.RoundToInt(_idleDestination.y)
                        )
                    ))
                {
                    _idleDestination = (Vector2)_transform.position + Random.insideUnitCircle * 10f;

                }*/


                var pos = _transform.position;

                Vector2Int nodePos = new Vector2Int(Mathf.CeilToInt(pos.x), Mathf.CeilToInt(pos.y));

                var lastPos = nodePos;
                while (World.Instance.voxelmap.IsAir(nodePos))
                {
                    lastPos = nodePos;
                    nodePos.y--;
                    isGrounded = false;
                }
                if (lastPos != nodePos)
                {
                    lastPos.y++;
                    isGrounded = true;
                }
                _idleDestination = lastPos;
            }
        }
    }
}