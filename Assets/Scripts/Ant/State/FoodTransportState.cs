﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ant
{
    public class FoodTransportState : BaseAntState
    {
        public override string Name => "Food Transport";
        private float timeSpentWalking;

        Task task;

        public FoodTransportState(AntController ant, Task foodTask) : base(ant)
        {
            task = foodTask;
            ant.GetComponent<Animator>().SetBool("Walking", true);
            ant.backpack.SetItem(0);
        }

        public override void OnStateEnter()
        {
            _ant.thoughts.AddThought(Phrases.CarryingFood);
            timeSpentWalking = 0;
        }

        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);

            timeSpentWalking += deltaTime;
            if (timeSpentWalking > Random.Range(2f, 6f))
            {
                _ant.thoughts.AddThought(Phrases.Walking);
                timeSpentWalking = 0f;
            }


            if (_ant.MoveTowards(task.position))
            {
                _ant.State = new WorkingState(_ant, _ant.CurrentTask);
            }
        }

        public override void OnStateExit()
        {
            base.OnStateExit();
            _ant.backpack.SetItem(-1);
        }
    }
}