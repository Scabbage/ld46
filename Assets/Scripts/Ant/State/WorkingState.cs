using UnityEngine;
using Queen;

namespace Ant
{
    public class WorkingState : BaseAntState
    {
        public override string Name => "Working";

        private float energyCostPerAction = 0.15f;

        public WorkingState(AntController ant, Task task) : base(ant)
        {
            _ant.CurrentTask = task;
            ant.GetComponent<Animator>().SetBool("Walking", false);
        }

        public override void OnStateEnter()
        {
            _ant.thoughts.AddThought(Phrases.Working);

            _ant._energy.AddEnergy(-(energyCostPerAction));
        }

        public override void Tick(float deltaTime)
        {
            if (_ant.CurrentTask.Completed)
            {
                _ant.CurrentTask = null;
                FindNewTask();
            }else if (_ant.CurrentTask.DoProgress(0.033f))
            {
                if (_ant.CurrentTask is World.FoodCollectionTask foodCollect)
                {
                    // bring food back to the queen
                    Task t = new Task();
                    t.position = _ant.transform.parent.position;
                    t.OnComplete = () =>
                    {
                        Queen.Queen.main.GetComponent<Energy>().AddEnergy(20f);
                    };
                    _ant.CurrentTask = t;
                    _ant.State = new FoodTransportState(_ant, t);
                } else
                {
                    FindNewTask();
                }
            }
        }
    }
}