using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// idle working walking
namespace Ant
{
	public class AntController : MonoBehaviour
	{
		const float taskShareAversion = 5f;

		private BaseAntState _state;

		public delegate void StateChangeHandler(BaseAntState newState);

		public event StateChangeHandler OnStateChange;

        public AntBackpack backpack;

		public BaseAntState State
		{
			get => _state;
			set
			{
				_state?.OnStateExit();
				_state = value;
				_selectable.State = _state;
				OnStateChange?.Invoke(_state);
				_state?.OnStateEnter();
			}
		}

		Task currentTask;

		public Task CurrentTask
		{
			get { return currentTask; }
			set
			{
				if (currentTask != null)
					currentTask.DeregisterWorker(this);

				currentTask = value;
				value?.RegisterWorker(this);
			}
		}

		private HashSet<Task> _discoveredTasks = new HashSet<Task>();
		public Vector2 Position => transform.position;
		public ThoughtLog thoughts;
		private Selectable _selectable;

		private LifeSpan _lifeSpan;
		private bool _isAliveInit = true;
		private float _time;
		private float _timeSinceGettingOldThoughts = Mathf.Infinity;
		public float dayLengthInSeconds = 300f;

        public bool isDrowning = false;
        public bool isFloating = false;

		public Energy _energy;
		private float _moveSpeed = 2f;
		public float scanRadius = 5;

		Stack<Vector2> currentPath;

		Vector2 currentDestination;

		private void OnEnable()
		{
			_energy = GetComponent<Energy>();
			_lifeSpan = GetComponent<LifeSpan>();
			_selectable = GetComponent<Selectable>();
			State = new IdleState(this);

            _selectable.nameString = PickRandomName();
		}

        /// <summary>
        /// Steps the ant towards the destination
        /// </summary>
        /// <param name="destination"></param>
        /// <returns>If the ant has reached the destination</returns>
        public bool MoveTowards(Vector2 destination)
		{
            Debug.DrawLine(transform.position, destination, Color.cyan);
            var offset = destination - (Vector2)transform.position;
            if (offset.magnitude < 1.5f)
            {
                //transform.Translate(offset * Time.deltaTime);
                return true;
            }

            if (currentPath == null || destination != currentDestination)
			{
                currentPath = World.Instance.pathfinder.GetPath(transform.position, destination);
                
                currentDestination = destination;
            }

            if (currentPath == null)
                return false;

            while (currentPath.Count > 0 && 
                Vector2.Distance(transform.position, destination) <
                Vector2.Distance(currentPath.Peek(), destination))
            {
                currentPath.Pop();
            }

            if(Vector2.Distance(transform.position, destination) < 1.5f)
                return true;

			Vector2 diff;
            if (currentPath?.Count > 0)
            {
                diff = currentPath.Peek() - Position;
            }
            else
            {
                diff = destination - Position;
            }

			float step = _moveSpeed * Time.deltaTime;
			if (diff.magnitude < step)
			{
				if (currentPath.Count > 0)
				{
					transform.position = currentPath.Peek();
					currentPath.Pop();
				}
				else
				{
					transform.position = destination;
					return true;
				}
			}
			else
			{
				transform.Translate(diff.normalized * step);
			}

			return false;
		}

		public Task[] CheckForTasks()
		{
			var nearbyTasks = TaskManager.Instance.FindTasksWithinRadius(transform.position, scanRadius).ToArray();
			foreach (Task task in nearbyTasks)
			{
				_discoveredTasks.Add(task);
			}

			return nearbyTasks;
		}

		public Task ClosestTask()
		{
			var tasks = CheckForTasks();
			float closestDist = float.MaxValue;
			Task closestTask = null;
			foreach (var task in tasks)
			{
                float distance = (task.position - (Vector2)transform.position).sqrMagnitude +
                                 task.WorkerCount * taskShareAversion
                                 - task.priority;
                // only take food collection tasks if it has no
                // workers assigned
                if(task.WorkerCount > 0 && task is World.FoodCollectionTask foodCollect)
                    continue;


				if (distance < closestDist)
				{
					closestDist = distance;
					closestTask = task;
				}
			}

			return closestTask;
		}

		private void TaskFound(Task task)
		{
			Debug.Log("Task found " + task);
		}

		void FixedUpdate()
		{
			_state?.Tick(Time.fixedDeltaTime);

			if (_isAliveInit)
			{
				_time += Time.fixedDeltaTime;
				_selectable.ageInDays = Mathf.FloorToInt(_time / dayLengthInSeconds);
			}

			if (_lifeSpan.isGettingOld && _isAliveInit)
			{
				_timeSinceGettingOldThoughts += Time.fixedDeltaTime;
				if (_timeSinceGettingOldThoughts > UnityEngine.Random.Range(15f, 30f))
				{
					thoughts.AddThought(Phrases.GettingOld);
					_timeSinceGettingOldThoughts = 0f;
				}
			}

            //MAKE ANTS DROWN IN WATER
            Vector3 thtransform = transform.position;
            int intx = (int)thtransform.x;
            int inty = (int)thtransform.y;
            int wetnessInt = World.Instance.voxelmap.Wetness(intx, inty);
            wetnessValue = wetnessInt;
            //int wetnessIntBelow = World.Instance.voxelmap.Wetness(intx, inty - 1);

            if (wetnessInt >= 9 && !isDrowning && _isAliveInit)
            {
                State = new StruggleState(this);
                isFloating = true;
                isDrowning = true;
            }
            
            if (wetnessInt <= 0 && isDrowning && _isAliveInit)
            {
                StartCoroutine("CheckIfOkayToIdle");
                isFloating = false;
                isDrowning = false;
            }


            FloatUpwards();
        }

		private void LateUpdate()
		{
			if (!_lifeSpan.isAlive && _isAliveInit)
			{
				State = new DeathState(this);
				_isAliveInit = false;
			}
            if (_isAliveInit && _energy.energy <= 0)
            {
                State = new DeathState(this);
                _isAliveInit = false;
            }

		}

        private int wetnessValue;

        IEnumerator CheckIfOkayToIdle()
        {
            yield return new WaitForSeconds(1f);
            if (wetnessValue <= 0 && isDrowning && _isAliveInit)
            {
                transform.Translate(Vector3.down * Time.deltaTime, Space.World);
            }
            yield return new WaitForSeconds(2f);
            if (!isDrowning && _isAliveInit && !isFloating)
            {
                State = new IdleState(this);
            }
        }

        void FloatUpwards()
        {
            
            if (isFloating && _isAliveInit)
            {
                transform.Translate(Vector3.up * Time.deltaTime, Space.World);
            }


        }

        private void OnDrawGizmos()
		{
			if (currentPath != null && currentPath.Count > 0)
			{
				Gizmos.color = Color.red;
				Vector2 last = currentPath.Pop();
				Stack<Vector2> temp = new Stack<Vector2>();
				temp.Push(last);

				while (currentPath.Count > 0)
				{
                    if (World.Instance.voxelmap.IsAir(new Vector2Int((int)last.x, (int)last.y)))
                        Gizmos.color = Color.blue;
                    else
                        Gizmos.color = Color.gray;
                    Gizmos.DrawWireSphere(last, 0.6f);

                    Gizmos.color = Color.red;
					Gizmos.DrawLine(last, currentPath.Peek());
					temp.Push(currentPath.Pop());
					last = temp.Peek();
				}


				while (temp.Count > 0)
				{
					currentPath.Push(temp.Pop());
				}
			}
		}


        string PickRandomName()
        {
            var random = new System.Random();
            var list = new List<string>
            {
                "Fred",
                "Greg",
                "Peanut",
                "Butthead",
                "Princess",
                "Beavis",
                "Mike",
                "Darren",
                "Josh",
                "Ben",
                "Benny",
                "Scooter",
                "Teddy",
                "Antony",
                "Corona",
                "Jesus",
                "Adolf",
                "Thea",
                "Jules",
                "Jester",
                "Zak",
                "Bud",
                "Dave",
                "Tim",
                "Steve",
                "Rio",
                "Tom",
                "Betty",
                "Brandon",
                "Brady",
                "Sally",
                "Karen",
                "Suzy",
                "Martha",
                "Sara",
                "Amelia",
                "Amy",
                "Kristen",
                "Cas",
                "Jake",
                "Kevin",
                "Oscar",
                "Bryan",
                "Sam",
                "Jerry",
                "Taylor",
                "Oz",
                "Yin",
                "Speedy",
                "Mack",
                "Jordan",
                "Kat",
                "Pam",
                "Ann",
                "Scott",
                "Hudson",
                "Casper",
                "Maggie",
                "Zeke",
                "Ellie",
                "Rob",
                "Jon",
                "Gus",
                "Frank",
                "Oliver",
                "Carl",
                "Sean",
                "Nathan",
                "Billy",
                "Gypsy",
                "Alex",
                "Billy",
                "Beck",
                "Bob",
                "Chris",
                "Darrel",
                "DJ",
                "Devon",
                "Ethan",
                "Emily",
                "Fabio",
                "Frank",
                "Rosa",
                "Olga",
                "Hellen",
                "Harry",
                "Indigo",
                "Jasmine",
                "Jessie",
                "Jamie",
                "James",
                "Kyle",
                "Liz",
                "Lemon",
                "Molly",
                "Matt",
                "Patsy",
                "Paul",
                "Peter",
                "Bucko",
                "Quin",
                "Sage",
                "Rachel",
                "Rusty",
                "Ricks",
                "Dink",
                "Samson",
                "Tiki",
                "Tin Tin",
                "Ursula",
                "Vince",
                "Vicky",
                "Winston",
                "Penis",
                "Fart",
                "Bubbles",
                "Poochie",
                "Cooter",
                "Wendy",
                "Yappy",
                "Yippie",
                "Ollie",
                "Laura",
                "Beth",
                "Zeb",
                "Zed",
                "Westside",
                "Nora",
                "Dump",
                "Slut",
                "Covid",
                "SARS",
                "Ludum",
                "HK47",
                "Crotch",
                "Juicy",
                "Moist",
                "Softy",
                "Flacid",
                "Erection",
                "Dildo",
                "Pete",
                "Trump",
                "Forky",
                "Spaz",
                "Ninja",
                "PewPeeDye",
                "Shroud",
                "Gerald",
                "Maggie",
                "Jordan",
                "Brook",
                "OJ",
                "Barney",
                "Robin",
                "Wash",
                "Dusty",
                "Brown",
                "Linda",
                "Slut",
                "Barb",
                "Carmack",
                "Dylan",
                "Craig",
                "Radcliff",
                "Potter",
                "Scooby",
                "Bush",
                "Obama",
                "Carter",
                "Washington",
                "Cleveland",
                "Denver",
                "Coach",
                "Champ",
                "Pimp",
                "Biff",
                "Maybe",
                "Jeb",
                "HAL",
                "R2D2",
                "C3P0",
                "Popsicle",
                "Nigel",
                "Callie",
                "Ken",
                "Barbie",
                "Poke",
                "Jap",
                "Texas",
                "Indie",
                "Bobcat",
                "Baskins",
                "Tiger",
                "Kreiger",
                "Archer"
            };
            int index = random.Next(list.Count);

            return list[index];
        }
    }
}