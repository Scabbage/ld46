﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ant
{
    public class LifeSpan : MonoBehaviour
    {

        public float lifeSpanInSeconds = 5f;
        private float initialLifeSpan;
        public float minLifeSpan = 250f;
        public float maxLifeSpan = 600f;

        public bool isAlive = true;
        public bool isGettingOld = false;

        // Start is called before the first frame update
        void OnEnable()
        {
            initialLifeSpan = Random.Range(minLifeSpan, maxLifeSpan);
            lifeSpanInSeconds = initialLifeSpan;
        }

        private void FixedUpdate()
        {
            lifeSpanInSeconds -= Time.fixedDeltaTime;
            if (lifeSpanInSeconds <= 0 && isAlive)
            {
                isAlive = false;
            }
            if (lifeSpanInSeconds <= (initialLifeSpan * 0.2f) && !isGettingOld && isAlive)
            {
                isGettingOld = true;
            }
        }

    }
}
