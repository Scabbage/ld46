﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntBackpack : MonoBehaviour
{
    public GameObject[] items;

    public void SetItem(int itemIdx)
    {
        for(int ii = 0; ii < items.Length; ii++)
        {
            if(ii == itemIdx)
            {
                if (!items[ii].activeSelf)
                    items[ii].SetActive(true);
            }else
            {
                if(items[ii].activeSelf)
                    items[ii].SetActive(false);
            }
        }
    }
}
