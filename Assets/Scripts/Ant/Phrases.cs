namespace Ant
{
    public static class Phrases
    {
        public static string Idle => _idle.Random;
        private static PhraseEvent _idle = new PhraseEvent(new string[]
        {
            "Looks like there's nothing else to do",
            "Carol Baskins killed her husband",
            "Epstein didn't kill himself",
            "I prefer social distANTing.",
            "We're probably distANT relatives.",
            "Ahh, time to sit on my gaster and relax.",
            "No more work.",
            "I'm gonna rest these legs for a bit.",
            "I ANTicipate repeating things to myself.",
            "Have you ever heard of Die Antword?",
            "Wish I had something to do...",
            "I'm bored",
            "Is there a god?",
            "I'm a small ant with big dreams.",
            "Hope the queen doesn't see me slacking.",
            "Nothing to see here.",
            "I should buy a boat",
            "What if we only think in memes?",
            "I deleted my ANTbook account.",
            "Social media is too ANTagonistic.",
            "Thinking about stuff.",
            "Standing idly by.",
            "Watching the world pass me by.",
            "Am I talking to myself?",
            "Feels like I'm being watched.",
            "Is someone there?",
            "Wonder if there's any food nearby",
            "Taking a break",
            "La la la, nothing to see here.",
            "I've got high hopes.",
            "Having high apple pie in the sky hopes",
            "Thinking of rubber tree plants",
            "Can't even right now.",
            "I'm only 2 degrees from Kevin BacANT.",
            "I'm on a strike.",
            "Shoulda joined the army ants.",
            "Maybe I'll go stand over there instead",
            "What's the deal with dirt anyway?",
            "Living every day like its my last.",
            "Has anyone really been far even ant...",
            "Such dirt, much ants.",
            "Unfair, the queen gets all the food.",
            "We do all the work, and SHE gets fed.",
            "Where's my army of servANTs?",
            "Wish I had some pants to dance in.",
            "Some ants are uncles.",
            "I'm king of the hill",
            "I hate the queen",
            "Army ants intimidate me.",
            "Long live the queen.",
            "I disagree with colonization",
            "Colonialism is imperialistic.",
            "Smell my pheremones.",
            "I guarANTee it.",
            "I've said it before, I guarANTee it.",
            "Remember there are more of us...",
            "I have no idea what I'm doing.",
            "Don't listen to me.",
            "I'm ANTi-colonialism.",
            "The movie ANTZ really speaks to me.",
            "Sometimes I have dirty dreams.",
            "... ... ...",
            "I'd vote ANTi-establishment",
            "I cANT even.",
            "One day I'll make a colony of my own.",
            "Who am I kidding, I'm just a dirty ant.",
            "I bet we could live on Mars.",
            "I sure know a lot of stuff for an ant.",
            "Hey you, yeah YOU!",
            "Between you and me...",
            "I'm auquaphobic.",
            "Having dirty thoughts.",
            "Let's go play in the dirt.",
            "Chillin' like a villain.",
            "Im one cool ant",
            "I have self esteem issues.",
            "I'm an ANTrovert.",
            "If you're seeing this, I'm insane.",
            "Thankfully noone can read my mind!",
            "Just me, myself, ant I."
        });

        public static string Walking => _walking.Random;
        private static PhraseEvent _walking = new PhraseEvent(new string[]
        {
            "Going for a walk",
            "Taking a hike",
            "Moving to a new location",
            "Hey! I'm walkin' here!",
            "I'm ANTicipating stuff over there.",
            "Skidaddling",
            "I'm going somewhere to do great things!",
            "They see me rollin' they hatin'",
            "Following my dreams... somewhere.",
            "It's my way or the highway.",
            "On a stroll.",
            "Left foot, left foot, left foot, right.",
            "On my way to go do something.",
            "Oh finally! Something to do.",
            "Heading to work now.",
            "Commuting.",
            "Walking to work.",
            "This is my way to the job site.",
            "Quit following me!",
            "Trying to get to work.",
            "Looking for my job site.",
            "Wandering towards work.",
            "Still walking.",
            "There's something to do over there.",
            "cANT stop me now!",
            "cANT sit still anymore."
        });

        public static string Working => _working.Random;
        private static PhraseEvent _working = new PhraseEvent(new string[]
        {
            "Working",
            "They don't feed me enough for this",
            "Gettin' er done",
            "Doing things that matter.",
            "Eatin' dirt.",
            "Do ants sweat? I think I'm sweating.",
            "Wow what a workout!",
            "Just keep digging!",
            "Digging...",
            "Tunnelling",
            "Carving caves",
            "Shoveling dirt down here.",
            "Making more holes in the ground.",
            "Hopefully not digging my own grave.",
            "Insert shovel emoji here.",
            "Workin that nine to five.",
            "Working from home.",
            "I better get paid overtime for this.",
            "I hope this is worth it.",
            "Okay boss, I trust you.",
            "Where the hell are we digging to?",
            "Why are we digging this way?",
            "Are you sure about this dig site?",
            "I hate digging...",
            "Digging, again.",
            "Do I LOOK like Stanley Yelnats?",
            "Dig it up uh uh yeah.",
            "I'm a gold digger, except it's dirt"
        });

        public static string CarryingFood => _carryingFood.Random;
        private static PhraseEvent _carryingFood = new PhraseEvent(new string[]
        {
            "Taking this food!",
            "I'm a food delivery dude!",
            "Haulin' this food.",
            "Got food on my back.",
            "The queen needs food!",
            "nom nom nom",
            "I hop I get a promotion for this.",
            "Feedin' the queen!",
            "This food is heavy",
            "Grabbin' this food for the queen."
        });

        public static string GettingOld => _gettingOld.Random;
        private static PhraseEvent _gettingOld = new PhraseEvent(new string[]
        {
            "These knees aren't what they used to be.",
            "I'm getting too old for this shit.",
            "I ANTicipate death's door approaching.",
            "I ain't young like I used to be.",
            "I feel old...",
            "I don't feel so good...",
            "On the stairway to heaven.",
            "On the highway to hell.",
            "It's time to draft my will.",
            "Why didn't I save for retirement?",
            "My life is passing before my eyes...",
            "Why am I in the mood to watch Jepardy?",
            "All my dreams are of death.",
            "The grim reaper is following me.",
            "It's time to dig my own grave...",
            "My demise isn't far off.",
            "The pearly gates await me soon.",
            "Feelin' like a senior citizen",
            "I've had a long life.",
            "I hope I die in my sleep.",
            "Shoulda bought a boat."
        });

        public static string Struggling => _struggling.Random;
        private static PhraseEvent _struggling = new PhraseEvent(new string[]
        {
            "I'm struggling!",
            "AHH I'm drowning!",
            "HELP I'M DYING!",
            "'Just keep swimming' my ass!",
            "Where's Rose and that door?",
            "I don't want it to end like this!",
            "Crap, I never learned how to swim!",
            "Treading water is tiring me out.",
            "Can't breathe",
            "This water is killing me.",
            "Someone get help, I've fallen in a well!",
            "I'll die if I don't get out of here",
            "Fading fast!",
            "Is anyone there? HELP ME!",
            "SAVE ME!",
            "I've fallen and I can't get up",
            "Call 911, its an emergency!",
            "Where's the gosh darn life guard?!"
        });


        public static string Death => _death.Random;
        private static PhraseEvent _death = new PhraseEvent(new string[]
        {
            "GOODBYE CRUEL WORLD!",
            "X.X",
            "I'm dead, if you couldn't tell.",
            "RIP",
            "Why did it have to end like this!?",
            "Dead",
            "Died doing what they loved.",
            "Died in a terrible accident.",
            "Here lies ant, he was one of a million.",
            "Chillin' with the grim reaper now.",
            "This is now a dead ant.",
            "A husk of a former ant.",
            "Obviously this ant died.",
            "Wouldn't have died if you had tried.",
            "So this is what death is like.",
            "Fell off Rose's door.",
            "Sleepin' with the fishes",
            "Kicked the bucket.",
            "Was assassinated by life.",
            "Ended up between rock and hard place.",
            "I'M DEAD YO!",
            "Decomposing...",
            "Ghosting life itself.",
            "this.ant.state = State(dead);",
            "Super duper dead.",
            "Bit the dust.",
            "Died because of you."
        });

        public static string QueenIdle => _queenIdle.Random;
        private static PhraseEvent _queenIdle = new PhraseEvent(new string[]
        {
            "Too tired to make babies.",
            "I ANTicipate repeating things to myself.",
            "I have a headache",
            "Is there anything to eat around here?",
            "It's time to order some take out food.",
            "How do you expect me to feed a child?",
            "Feeling feint, low blood sugar",
            "Need food.",
            "FEED ME!",
            "Seriously, get me some food already.",
            "Yes, I'll have fries with that.",
            "All I can think about is eating.",
            "Stress eating sounds good right now.",
            "Struggling as a single parent.",
            "Someone order me a pizza",
            "Go find food and bring it to me",
            "Hear that? Its my stomach growling.",
            "If you don't feed me soon I'm leaving.",
            "Feed me or watch me die.",
            "I'll haunt you with food."
        });
        
        public static string QueenStruggling => _queenStruggling.Random;
        private static PhraseEvent _queenStruggling = new PhraseEvent(new string[]
        {
            "YOUR QUEEN IS DYING!",
            "I'M DROWNING",
            "Get me out of this water!",
            "Obviously I CAN'T SWIM",
            "Not sure what's worse, this or starving",
            "AHHHHH",
            "Having trouble breathing",
            "Allergic to water",
            "If you couldn't tell, this is killing me",
            "This is killing me.",
            "Literally dying right now.",
            "I'll haunt you when it rains.",
            "Still can't swim...",
            "Unable to breathe",
            "I miss oxygen",
            "Won't survive this much longer.",
            "MAKE IT STOP!"
        });

        public static string QueenBirthing => _queenBirthing.Random;
        private static PhraseEvent _queenBirthing = new PhraseEvent(new string[]
        {
            "Pooping out baby ants",
            "Busy laying eggs.",
            "This is what real work looks like.",
            "You try raising a thousand children",
            "Oo... that last one won't last long.",
            "See that one? He's my favorite child.",
            "So what if I forget my kids name?",
            "Squeezing out another ant.",
            "Giving birth, what's it look like?",
            "Witness the miracle of child birth.",
            "I may have dropped one on it's head",
            "Oops, I did it again.",
            "I ANTicipate another ant on the way",
            "Making babies",
            "Delivering another ant.",
            "Trying natural child birth.",
            "I reduce stretch marks with coconut oil",
            "MILF: Mother I Love to Feed",
            "Cranking out more ants",
            "Better keep an eye on this next one.",
            "We're gonna need a bigger house.",
            "You try pushing one of these out!",
            "My body will never look the same...",
            "Octo-mom is a chump.",
            "Here comes another one",
            "Brewing babies",
            "The pope would be proud.",
            "How many is too many?",
            "You can never have enough kids!",
            "I may be tired of kids.",
            "Hopefully that one goes to college."
        });

        public static string QueenDeath => _queenDeath.Random;
        private static PhraseEvent _queenDeath = new PhraseEvent(new string[]
        {
            "YOUR QUEEN IS DEAD!",
            "Dead, meaning GAME OVER",
            "YOU HAD ONE JOB! X.X"
        });

        private struct PhraseEvent
        {
            public string Random => _phrases[UnityEngine.Random.Range(0, _phrases.Length)];
            private string[] _phrases;

            public PhraseEvent(string[] phrases)
            {
                _phrases = phrases;
            }
        }
    }
}