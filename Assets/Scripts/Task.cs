using Ant;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Task 
{
	public float Progress { get; private set; }

    public Vector2 position;

    public Action OnComplete;

    private HashSet<AntController> workers = new HashSet<AntController>();

    public int WorkerCount => workers.Count;

    public bool Completed => Progress > 1f;


    /// <summary>
    /// The priority of this task.
    /// Higher will increase the likelihood of ants choosing this task.
    /// If a task has priority x, then the next closest task will have to 
    /// be x units closer to an ant to be chosen.
    /// </summary>
    public float priority;

	public Task()
    {
		TaskManager.Instance.AddTask(this);
	}

	/// <summary>
	/// Does a little bit of progress by amount
	/// </summary>
	/// <returns>True if progress is done</returns>
	public bool DoProgress(float amount)
	{
        if (Progress > 1)
            return true;

        Progress += amount;
        if (Progress >= 1)
        {
            OnComplete?.Invoke();
            TaskManager.Instance.RemoveTask(this);
            return true;
        }
        return false;
        
	}

    public void RegisterWorker(AntController ac)
    {
        workers.Add(ac);
    }

    public void DeregisterWorker(AntController ac)
    {
        workers.Remove(ac);
    }
}