﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteFlipper : MonoBehaviour
{

    private SpriteRenderer spriteRenderer;
    private Transform currentPosition;
    private Vector3 lastPosition;
    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        
        currentPosition = GetComponent<Transform>();
        lastPosition = currentPosition.position;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (currentPosition.position.x > lastPosition.x)
        {
            spriteRenderer.flipX = false;
            lastPosition.x = currentPosition.position.x;
        }
        else if (currentPosition.position.x < lastPosition.x)
        {
            spriteRenderer.flipX = true;
            lastPosition.x = currentPosition.position.x;
        }
    }
}
