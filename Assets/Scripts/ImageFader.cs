﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageFader : MonoBehaviour
{
    public float FadeRate = 1f;
    public float initialDisplayTime = 5f;
    private Image image;
    private Text[] texts;
    private float targetAlpha;
    // Use this for initialization
    void Start()
    {
        this.texts = this.GetComponentsInChildren<Text>();
        this.image = this.GetComponent<Image>();
        if (this.image == null)
        {
            Debug.LogError("Error: No image on " + this.name);
        }
        this.targetAlpha = this.image.color.a;
        
        StartCoroutine("DelayedFade");
    }

    // Update is called once per frame
    void Update()
    {
        Color curColor = this.image.color;
        float alphaDiff = Mathf.Abs(curColor.a - this.targetAlpha);
        if (alphaDiff > 0.0001f)
        {
            curColor.a = Mathf.Lerp(curColor.a, targetAlpha, this.FadeRate * Time.deltaTime);
            this.image.color = curColor;
            foreach (Text text in texts)
            {
                text.color = curColor;
            }
        }
    }

    IEnumerator DelayedFade()
    {
        yield return new WaitForSeconds(initialDisplayTime);
        FadeOut();
    }

    public void FadeOut()
    {
        this.targetAlpha = 0.0f;
    }

    public void FadeIn()
    {
        this.targetAlpha = 1.0f;
    }
}
