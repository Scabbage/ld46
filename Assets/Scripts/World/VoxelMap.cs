﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.Threading;
using System.Threading.Tasks;

[RequireComponent(typeof(Tilemap))]
public class VoxelMap : MonoBehaviour
{
    public Tilemap waterMap;

    public Vector3Int size
    {
        get
        {
            return map.size;
        }
    }

    int[] voxels;
    int height, width;

    /// <summary>
    /// Info on how much water is in each tile.
    /// Concurrent read, exclusive write. 
    /// Don't take the lock if you are only reading, 
    /// it'll just lead to frame drops.
    /// </summary>
    int[] water;
    int[] waterCache;
    object waterLock = new object();
    int maxWaterPerCell = 16;
    int waterGravity = 2;

    public TileBase[] voxelMaterials;
    public TileBase[] waterMaterials;

    List<Vector2Int> waterUpdateQueue = new List<Vector2Int>();
    object waterQueueLock = new object();

    [Range(0f, 1f)]
    public double spreadJitter = 0.01d;

    [Range(0f, 0.1f)]
    public double rainSpeed = 0.001d;
    public int waterSimulationInterval = 10;

    [Range(0f, 1f)]
    public double evaporationRate = 0.1;

    Tilemap map;

    System.Threading.Tasks.Task waterTask;

    bool simulateWater = false;

    public bool rain;

    void Awake()
    {
        map = GetComponent<Tilemap>();
        Refresh();

        simulateWater = true;
        waterTask = new System.Threading.Tasks.Task(WaterUpdateTask);
        waterTask.Start();
    }

    void FixedUpdate()
    {
        DisplayWater();
    }

    public void Refresh()
    {
        if(width != map.size.x || height != map.size.y)
        {
            lock (waterLock)
            {
                width = map.size.x;
                height = map.size.y;
                voxels = new int[map.size.x * map.size.y];
                water = new int[voxels.Length];
                waterCache = new int[voxels.Length];
            }
        }

        Vector3Int pos = default;
        for (int yy = 0; yy < map.size.y; yy++)
        {
            for (int xx = 0; xx < map.size.x; xx++)
            {
                pos.x = xx;
                pos.y = yy;
                voxels[xx + yy * width] = map.GetTile(pos) == null? 0 : 1;
            }
        }
    }

    void WaterUpdateTask()
    {
        System.Random rng = new System.Random();
        int idx1, idx2;
        Vector2Int pos = default;
        try
        {
            while (simulateWater)
            {
                lock (waterLock)
                {
                    // rain
                    if (rain)
                    {
                        for (int xx = 0; xx < width; xx++)
                        {
                            if (rng.NextDouble() < rainSpeed)
                                AddWater(xx + (height - 1) * width, maxWaterPerCell);
                        }
                    }

                    // gravity
                    for (int yy = 0; yy < height - 1; yy++)
                    {
                        for (int xx = 0; xx < width; xx++)
                        {
                            pos.x = xx;
                            pos.y = yy;

                            if (IsAir(pos))
                            {
                                // take water from the cell above
                                idx1 = xx + yy * width;
                                idx2 = idx1 + width;
                                MoveWater(idx2, idx1, waterGravity);
                            }
                        }
                    }

                    // evaporation
                    for(int ii = 0; ii < water.Length; ii++)
                    {
                        if(rng.NextDouble() < evaporationRate)
                        {
                            Evaporate(ii);
                        }
                    }

                    // spread
                    for (int yy = 0; yy < height - 1; yy++)
                    {
                        for (int xx = 0; xx < width; xx++)
                        {
                            Spread(xx, yy, rng);
                        }
                    }

                    //ValidateWater();
                }

                //Thread.Sleep(waterSimulationInterval);
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e);
        }

        Debug.Log("Water simulation ended.");
    }

    void Spread(int x, int y, System.Random rng)
    {
        // only spread if the voxel below the water cell is 
        // solid
        if (y == 0 ||
            water[x + (y-1) * width] == maxWaterPerCell || 
            !IsAir(new Vector2Int(x, y-1)))
        {
            int localIdx = x + y * width;
            int otherIdx = localIdx - 1;
            // spread left
            if (x > 0)
            {
                if (water[otherIdx] < maxWaterPerCell &&
                    water[otherIdx] < water[localIdx] &&
                    IsAir(otherIdx) &&
                    rng.NextDouble() < spreadJitter)
                {
                    MoveWater(localIdx, otherIdx, 1);
                }
            }

            //spread right
            otherIdx = localIdx + 1;
            if (x < width)
            {
                if (water[otherIdx] < maxWaterPerCell &&
                    water[otherIdx] <= water[localIdx] &&
                    IsAir(otherIdx) &&
                    rng.NextDouble() < spreadJitter)
                {
                    MoveWater(localIdx, otherIdx, 1);
                }
            }
        }
    }

    void Evaporate(int idx)
    {
        if (water[idx] > 0)
            water[idx]--;
    }

    private void OnDestroy()
    {
        simulateWater = false;
    }

    void ValidateWater()
    {
        for(int ii = 0; ii < water.Length; ii++)
        {
            if(water[ii] < 0)
            {
                water[ii] = 0;
                Debug.Log("water < 0");
            }
        }
    }

    void AddWater(int targetIdx, int amount)
    {
        water[targetIdx] += amount;
        if(water[targetIdx] > maxWaterPerCell)
        {
            water[targetIdx] = maxWaterPerCell;
        }
    }

    void MoveWater(int sourceIdx, int targetIdx, int amount)
    {
        amount = Mathf.Min(Mathf.Min(
            maxWaterPerCell - water[targetIdx],
            water[sourceIdx]), 
            amount);

        water[sourceIdx] -= amount;
        water[targetIdx] += amount;
    }

    void DisplayWater()
    {
        int idx = 0;
        Vector3Int pos = default;
        for (int yy = 0; yy < height; yy++)
        {
            for (int xx = 0; xx < width; xx++)
            {
                pos.x = xx;
                pos.y = yy;
                if (waterCache[idx] != water[idx])
                {
                    waterCache[idx] = water[idx];
                    int waterIdx = (waterMaterials.Length - 1) * water[idx] / maxWaterPerCell;
                    if (waterIdx == 0 && water[idx] != 0)
                        waterIdx = 1;
                    waterMap.SetTile(pos, waterMaterials[waterIdx]);
                }
                idx++;
            }
        }
        
    }

    public bool TrySetVoxel(Vector3Int pos, int value)
    {
        int idx = pos.x + pos.y * width;
        if (idx < 0 || idx >= voxels.Length)
        {
            return false;
        }

        if (value < 0 || value >= voxelMaterials.Length)
        {
            return false;
        }

        voxels[idx] = value;
        map.SetTile(pos, voxelMaterials[value]);
        return true;
    }

    public bool TrySetVoxel(Vector2Int pos, int value)
    {
        int idx = pos.x + pos.y * width;
        if (idx < 0 || idx >= voxels.Length)
        {
            return false;
        }

        if (value < 0 || value >= voxelMaterials.Length)
        {
            return false;
        }

        voxels[idx] = value;
        Vector3Int v3Pos = new Vector3Int(pos.x, pos.y, 0);

        map.SetTile(v3Pos, voxelMaterials[value]);
        return true;
    }

    public bool TryGetVoxel(Vector2Int pos, out int v)
    {
        v = -1;
        int idx = pos.x + pos.y * width;
        if (idx < 0 || idx >= voxels.Length)
        {
            return false;
        }

        v = voxels[idx];
        return true;
    }

    public int GetVoxel(Vector2Int pos)
    {
        int idx = pos.x + pos.y * width;
        if (idx < 0 || idx >= voxels.Length)
        {
            return 0;
        }
        return voxels[idx];
    }

    public bool IsAir(int voxelIdx)
    {
        if (voxelIdx < 0 || voxelIdx >= voxels.Length)
            return false;

        int v = voxels[voxelIdx];
        return v == 0 || v == 6;
    }

    public int Wetness(int x, int y)
    {
        if (x > 0 && x < width && y > 0 && y < height)
        {
            int idx = x + y * width;
            return water[idx];
        }
        return 0;
    }

    public bool IsAir(Vector2Int pos)
    {
        int v = GetVoxel(pos);
        return v == 0 || v == 6;
    }
}
