﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class World : MonoBehaviour
{
    const int AIR_ID = 0;
    
    const int DIRT_0_ID = 1;
    const int DIRT_1_ID = 2;
    const int DIRT_2_ID = 3;
    const int DIRT_3_ID = 4;
    const int ROCK_ID = 5;
    const int CAVE_ID = 6;
    const int FOOD_ID = 7;

    public VoxelMap voxelmap;

    public float groundNoiseHeight = 3f;
    public float groundNoiseScale = 3f;
    public float groundNoiseFrequency = 0.1f;
    public float groundNoisePersistence = 0.6f;
    public float bedrockThickness = 2f;
    public float tunnelSize = 5f;

    public static World Instance { get; private set; }

    public ColonyMap map;
    public Astar pathfinder { get; private set; }

    [Range(0f, 0.1f)]
    public float tickInterval = 0.01f;

    HashSet<Vector3Int> exposedTiles = new HashSet<Vector3Int>();


    Dictionary<Vector3Int, Task> tileDigTasks = new Dictionary<Vector3Int, Task>();

    public int exposedTileCount, taskCount;

    float foodTimer = 1f;

    private class TileMineTask : Task
    {
        public Vector3Int tile;
        World world;

        public TileMineTask(World w, Vector3Int tile)
        {
            position.Set(tile.x + 0.5f, tile.y + 0.5f);
            this.tile = tile;
            world = w;
            OnComplete += TileMined;
        }

        void TileMined()
        {
            world.voxelmap.TrySetVoxel(tile, 6);
            world.pathfinder.Refresh((Vector2Int)tile);
            world.tileDigTasks.Remove(tile);
            //Debug.Log($"Mined tile at {tile}");
        }
    }

    public class FoodCollectionTask : Task
    {
        Vector2Int tile;
        World world;

        public FoodCollectionTask(World w, Vector2Int tile)
        {
            position = tile;
            world = w;
            this.tile = tile;
            OnComplete += TileMined;
        }

        void TileMined()
        {
            TaskManager.Instance.RemoveTask(this);
            world.voxelmap.TrySetVoxel(tile, 0);
            world.pathfinder.Refresh(tile);
        }
    }

    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            pathfinder = new Astar(voxelmap);
            Generate();
            StartCoroutine(Tick());
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        exposedTileCount = exposedTiles.Count;
        taskCount = tileDigTasks.Count;

        foodTimer -= Time.deltaTime;
        if(foodTimer <= 0f)
        {
            foodTimer = Random.Range(15f, 40f);
            SpawnFood();
            Debug.Log("Spawned food");
        }
    }

    IEnumerator Tick()
    {
        Vector3Int pos = default;

        //int pathfindingRefreshCounter = 0;
        int x=0, y=0;
        int exposedUpdateCount = 100;
        
        while (true)
        {
            // Exposed tile update
            for (int ii = 0; ii < exposedUpdateCount; ii++)
            {
                x++;
                if (x >= voxelmap.size.x)
                {
                    x = 0;
                    y++;
                    if (y >= voxelmap.size.y)
                        y = 0;
                }
                pos.Set(x, y, 0);

                /*
                if (IsExposed((Vector2Int)pos))
                    exposedTiles.Add(pos);
                else
                    exposedTiles.Remove(pos);
                    */
                if (!voxelmap.IsAir(new Vector2Int(pos.x, pos.y)) && map.ShouldBeAir(pos))
                {
                    if (!tileDigTasks.ContainsKey(pos))
                    {
                        // there is no task to dig it, so make one:
                        var task = new TileMineTask(this, pos);
                        tileDigTasks.Add(pos, task);
                        //Debug.Log("Adding task for tile destruction");
                    }
                }
            }
            Debug.DrawLine(pos, pos+Vector3Int.one, Color.blue);
            yield return new WaitForSeconds(tickInterval);

            /*
            //Debug.Log("tile dig update");
            // Tile dig task update
            foreach(var tile in exposedTiles)
            {
                if (!cache.TileIsEmpty((Vector2Int)tile))
                {
                    // Tile is not empty

                    
                }
            }

            yield return new WaitForSeconds(tickInterval);
            */

            /*
            pathfindingRefreshCounter++;
            if(pathfindingRefreshCounter > 100)
            {
                pathfindingRefreshCounter = 0;
                yield return new WaitForSeconds(tickInterval);
            }
            */
        }
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    /// <summary>
    /// Returns true if the given tile is both solid, and one of the neighbours is air.
    /// </summary>
    /// <param name="tile"></param>
    /// <returns></returns>
    bool IsExposed(Vector2Int tile)
    {
        if(!voxelmap.IsAir(tile))
        {
            tile.x++;
            if (voxelmap.IsAir(tile))
                return true;

            tile.x -= 2;
            if (voxelmap.IsAir(tile))
                return true;

            tile.x++;

            tile.y++;
            if (voxelmap.IsAir(tile))
                return true;

            tile.y -= 2;
            if (voxelmap.IsAir(tile))
                return true;
        }
        return false;
    }

    public void SpawnFood()
    {
        // Get food position

        Vector2Int foodPos = default;
        foodPos.x = Random.Range(0, 256);
        foodPos.y = (int)GroundHeight(foodPos.x) + 2;

        while(voxelmap.IsAir(foodPos))
        {
            foodPos.y--;
        }
        foodPos.y++;

        if(voxelmap.TrySetVoxel(foodPos, FOOD_ID))
        {
            FoodCollectionTask f = new FoodCollectionTask(this, foodPos);
            f.priority = 5f;
            TaskManager.Instance.AddTask(f);
        }
    }

    void Generate()
    {
        var mapSize = voxelmap.size;
        float groundHeight;
        Vector3Int pos = default;
        for (int xx = 0; xx < mapSize.x; xx++)
        {
            for (int yy = 0; yy < mapSize.y; yy++)
            {
                pos.x = xx;
                pos.y = yy;
                groundHeight = GroundHeight(xx);
                if (yy < bedrockThickness)
                {
                    voxelmap.TrySetVoxel(pos, ROCK_ID);
                }
                else if (yy < groundHeight * 0.25f)
                {
                    voxelmap.TrySetVoxel(pos, DIRT_3_ID);
                }
                else if (yy < groundHeight * 0.5f)
                {
                    voxelmap.TrySetVoxel(pos, DIRT_2_ID);
                }
                else if (yy < groundHeight * 0.75f)
                {
                    voxelmap.TrySetVoxel(pos, DIRT_1_ID);
                }
                else if (yy < groundHeight)
                {
                    voxelmap.TrySetVoxel(pos, DIRT_0_ID);
                }
            }
        }



        // Create the entrance to the colony
        Vector2 entrance = new Vector2(127.5f, GroundHeight(127) - 20f);
        
        map = new ColonyMap(voxelmap, entrance, tunnelSize);

        Vector2 chamberPos = entrance;
        Vector2 chamberSize = new Vector2(8f, 3f);
        //chamberPos.y -= 20f; // depth under the ground
        chamberPos.x += chamberSize.x * 3f;
        map.AddChamber(chamberPos, chamberSize);

        /*
        chamberPos.y += 10f;
        chamberPos.x -= 60f;
        chamberSize.x = 20f;
        map.AddChamber(chamberPos, chamberSize);
        */
    }

    /// <summary>
    /// Returns the ground height at the given column when the world is generated.
    /// </summary>
    /// <returns></returns>
    float GroundHeight(int x)
    {
        float h = x / 256f;

        float res = 
            200f 
            + groundNoiseHeight * Noise(h * groundNoiseScale, 4, groundNoiseFrequency, groundNoisePersistence);
        return res;
    }

    float Noise(float x, int octaves, float lacunarity, float persistence)
    {
        float amp = 0.5f;
        float res = 0.5f;
        for(int ii = 0; ii < octaves; ii++)
        {
            res += amp * Mathf.Sin(x);
            x *= lacunarity;
            amp *= persistence;
        }
        return res;
    }

    public void OnDrawGizmos()
    {
        if(map != null)
        {
            map.DebugDraw();

            Gizmos.color = Color.red;
            foreach(Task task in tileDigTasks.Values)
            {
                Gizmos.DrawSphere(task.position, 0.1f);
            }
        }
    }
}
