﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherController : MonoBehaviour
{
    /// <summary>
    /// The length of the day, in seconds;
    /// </summary>
    public float dayLength = 180f;

    /// <summary>
    /// The length of the night, in seconds.
    /// </summary>
    public float nightLength = 120f;

    /// <summary>
    /// The duration in seconds that sunset and sunrise take to transition.
    /// </summary>
    public float colorTransitionTime = 20f;


    public float timeTillFirstRain = 300f;

    public float minRainDuration, maxRainDuration;
    public float minDroughtDuration, maxDroughtDuration;

    public float dayEvaporation, nightEvaporation;

    public Color32 dayColor;
    public Color32 sunsetColor;
    public Color32 nightColor;

    float dayCycleCounter;

    float rainTime;

    Camera cam;

    public enum DayState
    {
        Day     = 0,
        Sunset  = 1,
        Night   = 2,
        Sunrise = 3
    }

    bool raining = false;

    public DayState dayState;

    void Start()
    {
        cam = Camera.main;
        ResetTime();
        rainTime = timeTillFirstRain;
        cam.backgroundColor = dayColor;
    }

    void Update()
    {
        dayCycleCounter -= Time.deltaTime;

        if (dayCycleCounter < colorTransitionTime)
        {
            if (dayCycleCounter < 0f)
            {
                // iterate state
                dayState = dayState.Next();
                if (dayState == DayState.Day)
                    World.Instance.voxelmap.evaporationRate = dayEvaporation;
                else
                    World.Instance.voxelmap.evaporationRate = nightEvaporation;

                ResetTime();
                cam.backgroundColor = GetColor(dayState);
            } else
            {
                cam.backgroundColor = Color32.Lerp(
                    GetColor(dayState.Next()),
                    GetColor(dayState),
                    dayCycleCounter / colorTransitionTime
                    );
            }
        }

        rainTime -= Time.deltaTime;

        if(rainTime <= 0)
        {
            raining = !raining;
            World.Instance.voxelmap.rain = raining;

            if (raining)
            {
                rainTime = Random.Range(minRainDuration, maxRainDuration);
            }
            else
            {
                rainTime = Random.Range(minDroughtDuration, maxDroughtDuration);
            }
        }

    }

    void ResetTime()
    {
        switch (dayState)
        {
            case DayState.Day:
                dayCycleCounter = dayLength;
                break;
            case DayState.Night:
                dayCycleCounter = dayLength;
                break;
            case DayState.Sunrise:
            case DayState.Sunset:
                dayCycleCounter = colorTransitionTime;
                break;
        }

    }

    Color32 GetColor(DayState state)
    {
        switch (state)
        {
            case DayState.Day:
                return dayColor;
            case DayState.Night:
                return nightColor;
            case DayState.Sunrise:
            case DayState.Sunset:
                return sunsetColor;
        }
        throw new System.ArgumentException();
    }

}

public static class DayStateExtensions
{
    public static WeatherController.DayState Next(this WeatherController.DayState state)
    {
        int stateInt = (int)state + 1;
        stateInt %= 4;
        return (WeatherController.DayState)stateInt;

    }

    public static bool IsTransition(this WeatherController.DayState state)
    {
        return ((int)state) % 2 == 0;
    }
}