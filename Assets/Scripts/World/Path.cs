﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path
{
    List<Vector2> positions;
    Vector2 destination;

    public int Count
    {
        get
        {
            return positions.Count;
        }
    }

    public Path(Stack<Vector2> positions)
    {
        int size = positions.Count;
        this.positions = new List<Vector2>(size);


        for(int ii = size - 1; ii > 0; ii--)
        {
            this.positions.Add(positions.Pop());
        }

        this.positions.Reverse();

        destination = this.positions[0];
    }

    public Vector2 GetNextPosition()
    {
        if(positions.Count > 0)
            return positions[positions.Count - 1];

        return destination;
    }

    public void RemoveNextPosition()
    {
        if(positions.Count > 0)
        {
            positions.RemoveAt(positions.Count - 1);
        }
    }

    public void DebugDraw()
    {
        if (positions.Count > 1)
        {
            for (int ii = 1; ii < positions.Count; ii++)
            {
                Gizmos.DrawLine(positions[ii], positions[ii-1]);
            }
        }
    }
}