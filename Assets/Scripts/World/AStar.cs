﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Linq;
using System.Text;
using UnityEngine.Tilemaps;


public class Astar
{
    const int MAX_ITERATIONS = 200;

    public class Node
    {
        public readonly Vector2 pos;
        public readonly bool isWalkable;

        public Node(Vector2 pos, bool walkable)
        {
            this.pos = pos;
            this.isWalkable = walkable;
        }

        public float Heuristic(Vector2 pos)
        {
            return (this.pos - pos).sqrMagnitude;
        }
    }

    VoxelMap grid;

    Node[] nodes;
    int width, height;

    Dictionary<Node, float> fScore = new Dictionary<Node, float>();
    Dictionary<Node, float> gScore = new Dictionary<Node, float>();
    Dictionary<Node, Node> cameFrom = new Dictionary<Node, Node>();
    List<Node> neighbours = new List<Node>();

    float beelineDistance = 3f;

    public Astar(VoxelMap grid)
    {
        this.grid = grid;
        Refresh();
    }
    

    /// <summary>
    /// Reloads tile data.
    /// </summary>
    public void Refresh()
    {
        width = grid.size.x;
        height = grid.size.y;
        nodes = new Node[width * height];
        //Debug.Log($"Created grid of width {width} and height {height}");
        Node n;
        TileBase tile;
        Vector3Int pos = default;
        for (int yy = 0; yy < height; yy++)
        {
            for (int xx = 0; xx < width; xx++)
            {
                pos.x = xx;
                pos.y = yy;

                n = new Node(new Vector2(xx, yy), grid.IsAir((Vector2Int)pos));
                nodes[xx + yy * width] = n;
            }
        }
    }

    public Stack<Vector2> GetPath(Vector2 start, Vector2 end)
    {
        Node startNode = GetNode(start);
        Node endNode = GetNode(end);

        if(Vector2.Distance(start, end) < beelineDistance)
        {
            return Beeline(start, end);
        }

        if (startNode == null || endNode == null)
            throw new InvalidOperationException($"Start or end out of bounds. Start: {start}, end: {end}");

        List<Node> openSet = new List<Node>();

        cameFrom.Clear();
        fScore.Clear();
        gScore.Clear();

        openSet.Add(startNode);

        gScore[startNode] = 0f;

        fScore[startNode] = startNode.Heuristic(end);

        Node current;

        float tentativeG;
        int iteration = 0;
        while(openSet.Count > 0 && iteration < MAX_ITERATIONS)
        {
            current = LowestF(openSet, fScore);

            /*
            if(current == endNode)
            {
                //return Path(cameFrom, current);
            }*/

            openSet.Remove(current);
            GetNeighbours(current, neighbours);
            
            foreach (var neighbour in neighbours)
            {
                // This is a modification to the A* algorithm which allows
                // for the final voxel to be solid.
                if (neighbour == endNode || neighbour.isWalkable)
                {
                    // d(current,neighbor) is the weight of the edge from current to neighbor
                    // tentative_gScore is the distance from start to the neighbor through current
                    if (!fScore.ContainsKey(neighbour))
                        fScore[neighbour] = float.MaxValue;

                    if (!gScore.ContainsKey(neighbour))
                        gScore[neighbour] = float.MaxValue;

                    tentativeG = gScore[current] + 1f +
                        (neighbour.isWalkable ? 0f : 50f);

                    if (tentativeG < gScore[neighbour])
                    {
                        // This path is better than any previous one, overwrite:
                        cameFrom[neighbour] = current;
                        gScore[neighbour] = tentativeG;
                        fScore[neighbour] = gScore[neighbour] + neighbour.Heuristic(end)
                            + (neighbour.isWalkable ? 0f : 50f);
                        if (!openSet.Contains(neighbour))
                        {
                            openSet.Add(neighbour);
                        }
                    }
                }
            }
            iteration++;
        }

        if(cameFrom.ContainsKey(endNode))
            return Path(cameFrom, endNode);

        return Beeline(start, end);
    }

    Stack<Vector2> Beeline(Vector2 start, Vector2 end)
    {
        var res = new Stack<Vector2>();
        res.Push(end);
        res.Push(start);
        return res;
    }

    Stack<Vector2> Path(Dictionary<Node, Node> parents, Node end)
    {
        Stack<Vector2> result = new Stack<Vector2>();
        Node next = end;
        Vector2 offset = new Vector2(0.5f, 0.5f);

        while(next != null)
        {
            result.Push(next.pos + offset);
            if (!parents.TryGetValue(next, out next))
                next = null;
        }
        return result;
    }

    Node LowestF(List<Node> nodes, Dictionary<Node, float> fScores)
    {
        if (nodes.Count == 0)
            throw new ArgumentException("No nodes in set.");

        float lowestVal = float.MaxValue;
        int lowest = -1;
        float temp;
        for(int ii = 0; ii < nodes.Count; ii++)
        {
            temp = fScores[nodes[ii]];
            if (temp < lowestVal)
            {
                lowest = ii;
                lowestVal = temp;
            }
        }
        return nodes[lowest];
    }

    public Node GetNode(Vector2 pos)
    {
        int x, y;
        x = Mathf.RoundToInt(pos.x);
        y = Mathf.RoundToInt(pos.y);

        if (x < 0 || y < 0 || x >= width || y >= height)
            throw new ArgumentException($"Out of bounds: {x}, {y}. W: {width}, H: {height}");

        return nodes[x + y * width];
    }

    Node GetNode(int x, int y)
    {
        if (x < 0 || y < 0 || x >= width || y >= height)
            return null;

        return nodes[x + y * width];
    }

    void GetNeighbours(Node node, List<Node> list)
    {
        list.Clear();
        int x = Mathf.RoundToInt(node.pos.x);
        int y = Mathf.RoundToInt(node.pos.y);
        Node n;

        n = GetNode(x+1, y);
        //if (n.isWalkable)
            list.Add(n);

        n = GetNode(x-1, y);
        //if (n.isWalkable)
            list.Add(n);

        n = GetNode(x, y+1);
        //if (n.isWalkable)
            list.Add(n);

        n = GetNode(x, y-1);
        //if (n.isWalkable)
            list.Add(n);
    }

    public void Refresh(Vector2Int cell)
    {
        var n = new Node(new Vector2(cell.x, cell.y), grid.IsAir(cell));
        nodes[cell.x + cell.y * width] = n;
    }
}