﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ColonyMap
{

    private class Node
    {
        public readonly Vector2 position;
        public readonly Vector2 bounds;
        private List<Node> neighbours = new List<Node>();

        public Node(Vector2 pos, Vector2 bounds)
        {
            this.position = pos;
            this.bounds = bounds;
        }

        public bool InBounds(Vector2 pos)
        {
            if (pos.x > position.x - bounds.x &&
                pos.x < position.x + bounds.x &&
                pos.y > position.y - bounds.y &&
                pos.y < position.y + bounds.y)
            {
                float h = bounds.y * 2f;
                Vector2 corner = position;
                corner.x += bounds.x;
                corner.x -= h;
                corner.y -= bounds.y;
                if (pos.x > corner.x)
                {
                    // round off the top corners

                    if (Vector2.Distance(pos, corner) > h)
                        return false;
                }

                corner = position;
                corner.x -= bounds.x;
                corner.x += h;
                corner.y -= bounds.y;
                if (pos.x < corner.x)
                {
                    if (Vector2.Distance(pos, corner) > h)
                        return false;
                }

                return true;
            }
            return false;
        }

        public bool InBounds(Vector2 pos, Vector2 halfAxis)
        {
            Vector2 lMin = position - bounds;
            Vector2 lMax = position + bounds;
            Vector2 oMin = pos - halfAxis;
            Vector2 oMax = pos + halfAxis;

            if ((oMax.x >= lMin.x && oMin.x <= lMax.x) &&
                (oMax.y >= lMin.y && oMin.y <= lMax.y))
                return true;

            return false;
        }


        /// <summary>
        /// Returns true if the given position is inside this node, or any nodes connected to it.
        /// </summary>
        /// <returns></returns>
        public static bool InGraphBounds(Vector2 pos, Vector2 halfAxis, HashSet<Node> nodes)
        {
            foreach (var node in nodes)
            {
                if (node.InBounds(pos, halfAxis))
                    return true;
            }

            return false;
        }

        public bool InGraphBounds(Vector2 pos, HashSet<Node> nodes)
        {
            foreach (var node in nodes)
            {
                if (node.InBounds(pos))
                    return true;
            }

            return false;
        }


        public static Node ClosestNode(Vector2 pos, HashSet<Node> nodes)
        {
            float currentDistance;
            float closestDistance = float.MaxValue;
            Node closest = null;
            foreach (Node current in nodes)
            {
                currentDistance = Vector2.SqrMagnitude(current.position - pos);
                if (currentDistance < closestDistance)
                {
                    closestDistance = currentDistance;
                    closest = current;
                }
            }
            return closest;
        }

        public void AddNeighbour(Node newChild)
        {
            if (!neighbours.Contains(newChild))
                neighbours.Add(newChild);
        }

        /// <summary>
        /// Connects 2 nodes together. If the distanc exceeds the maximum, more nodes will be generated to
        /// fill the gap.
        /// </summary>
        /// <param name="n1"></param>
        /// <param name="n2"></param>
        /// <param name="tunnelWidth"></param>
        public static void Connect(Node n1, Node n2, float tunnelWidth, HashSet<Node> nodeList = null)
        {
            float distance = Distance(n1, n2);
            if (distance > tunnelWidth / 2f)
            {
                // Get the number of new nodes that must be generated
                int newNodes = Mathf.CeilToInt(distance / (tunnelWidth / 2f));
                Node last = n1;
                Vector2 os = n2.position - n1.position;
                float temp;

                temp = os.x;
                os.x = os.y;
                os.y = -temp;

                os.Normalize();
                for (int ii = 1; ii <= newNodes; ii++)
                {
                    temp = (float)ii / newNodes;
                    Node tempNode = new Node(
                            os * 0.5f * Mathf.Sin(temp * 2f * tunnelWidth)
                            + Vector2.Lerp(n1.position, n2.position, temp),
                            new Vector2(tunnelWidth / 2f, tunnelWidth / 2f)
                        );
                    Connect(tempNode, last, float.MaxValue, nodeList);
                    last = tempNode;
                }
                Connect(n2, last, float.MaxValue);
                //Debug.Log($"Generated {newNodes} new nodes.");
            }
            else
            {
                // Simply add both nodes as each others neighbour
                n1.AddNeighbour(n2);
                n2.AddNeighbour(n1);
                if (nodeList != null)
                {
                    nodeList.Add(n1);
                    nodeList.Add(n2);
                }
            }
        }

        public static float Distance(Node n1, Node n2)
        {
            return Vector2.Distance(n1.position, n2.position);
        }

        public void DebugDraw()
        {
            Gizmos.DrawSphere(position, 0.1f);
            Gizmos.DrawWireCube(position, bounds * 2f);
            foreach (var neighbour in neighbours)
            {
                Debug.DrawLine(position, neighbour.position);
            }
        }
    }

    public float tunnelWidth;

    Node root;
    HashSet<Node> nodes = new HashSet<Node>();
    VoxelMap tiles;

    Vector2 TunnelBounds => new Vector2(tunnelWidth / 2f, tunnelWidth / 2f);

    public ColonyMap(VoxelMap tiles, Vector2 rootPos, float tunnelWidth)
    {
        if (tunnelWidth < 0f)
            tunnelWidth *= -1f;

        this.tiles = tiles;
        this.tunnelWidth = tunnelWidth;

        root = new Node(
            rootPos,
            new Vector2(tunnelWidth / 2f, tunnelWidth / 2f));

        nodes.Add(root);
    }

    /// <summary>
    /// Returns true if the given location can be added to the colony, false otherwise.
    /// </summary>
    /// <param name="halfAxis">The per-axis radii of the bounding box. Equivalent to half the width (for x) and half the height (for y).</param>
    /// <param name="position"></param>
    /// <returns></returns>
    public bool CanAddChamber(Vector2 position, Vector2 halfAxis)
    {
        return !Node.InGraphBounds(position, halfAxis, nodes);
    }

    public Vector2 ClosestPointInColony(Vector2 pos)
    {
        return ClosestNode(pos).position;
    }

    public void AddChamber(Vector2 position, Vector2 halfAxis)
    {
        if (!CanAddChamber(position, halfAxis))
        {
            throw new System.ArgumentException("Cannot add chamber, as it intersects with another.");
        }

        Vector2 attach1, attach2;

        attach1 = position - halfAxis;
        attach1.y += tunnelWidth / 2f;
        attach2 = attach1;
        attach2.x += halfAxis.x * 2f;

        Node closest1, closest2;
        closest1 = ClosestNode(attach1);
        closest2 = ClosestNode(attach2);

        Node chamber, left, right;
        chamber = new Node(position, halfAxis);
        left = new Node(attach1, TunnelBounds);
        right = new Node(attach2, TunnelBounds);
        Node.Connect(chamber, left, float.MaxValue);
        Node.Connect(chamber, right, float.MaxValue);

        Node closestNode, attachPoint;

        if ((attach1 - closest1.position).sqrMagnitude <
            (attach2 - closest2.position).sqrMagnitude)
        {
            attachPoint = left;
            closestNode = closest1;
        }
        else
        {
            attachPoint = right;
            closestNode = closest2;
        }

        Node.Connect(attachPoint, closestNode, tunnelWidth, nodes);

        nodes.Add(left);
        nodes.Add(right);
        nodes.Add(chamber);
    }

    public bool ShouldBeAir(Vector3Int tilePos)
    {
        Vector2 pos = new Vector2(tilePos.x, tilePos.y);
        pos.x += 0.5f;
        pos.y += 0.5f;

        foreach (Node n in nodes)
        {
            if (n.InBounds(pos))
                return true;
        }
        return false;
    }

    Node ClosestNode(Vector2 pos)
    {
        return Node.ClosestNode(pos, nodes);
    }

    public void DebugDraw()
    {
        Gizmos.color = Color.green;
        foreach (var n in nodes)
        {
            n.DebugDraw();
        }
    }
}
