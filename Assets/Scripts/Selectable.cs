using Ant;
using UnityEngine;

public class Selectable : MonoBehaviour
{
	public IState State;
	public int ageInDays { get; set; }
    public string nameString = "Ant";

	public ThoughtLog thoughtLog { get; private set; }
	private Transform _transform;
	public Vector2 Position => transform.position;

	private void OnEnable()
	{
		_transform = GetComponent<Transform>();
		thoughtLog = GetComponent<ThoughtLog>();
	}
}