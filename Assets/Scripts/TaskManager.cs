using System;
using System.Collections.Generic;
using UnityEngine;

public class TaskManager
{
	private static TaskManager _instance;

	public static TaskManager Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new TaskManager();
			}

			return _instance;
		}
	}

	private List<Task> _availableTasks = new List<Task>();
	
	private List<Task> _workingList = new List<Task>();

	public Task[] FindTasksWithinRadius(Vector2 origin, float radius)
	{
		_workingList.Clear();
		foreach (var task in _availableTasks)
		{
			float distance = (task.position - origin).magnitude;
			if (distance < radius)
			{
				_workingList.Add(task);
			}
		}

		return _workingList.ToArray();
	}

	public void AddTask(Task task)
	{
		_availableTasks.Add(task);
	}

    public bool RemoveTask(Task task)
    {
        return _availableTasks.Remove(task);
    }
}