﻿using System;
using Ant;
using UI;
using UnityEngine;

namespace AntCamera
{
	public class CameraController : MonoBehaviour
	{
		private BaseCameraState _state;

		public BaseCameraState State
		{
			get => _state;
			set
			{
				_state?.OnStateExit();
				_state = value;
				_state?.OnStateEnter();
			}
		}

		private float _zOffset;
		public Vector2 Position
		{
			get => _transform.position;
			set => _transform.position = new Vector3(value.x, value.y, _zOffset);
		}

		private Transform _transform;
		private Camera _camera;

        Vector4 bounds = new Vector4(0f, 0f, 256f, 256);
		
		public int minZoomLevel = 1;
		public float maxZoomLevel = 10;
		public float zoomSpeed = 2;
		public float zoomLevel = 22;

		private void OnEnable()
		{
			_transform = GetComponent<Transform>();
			_zOffset = _transform.position.z;
			_state = new FreeState(this);
		}

		private void Update()
		{
			_state?.Tick(Time.unscaledDeltaTime);

            KeepInBounds();
		}

        void KeepInBounds()
        {
            Vector3 pos = transform.position;
            Vector2 viewport;
            viewport.y = Camera.main.orthographicSize;
            viewport.x = viewport.y * Camera.main.aspect;
            Vector3 newPos;
            newPos = pos;
            if (pos.x < bounds[0] + viewport.x)
            {
                //Debug.Log($"XMin {newPos.x}, {viewport.x}, {bounds[0]}");
                newPos.x = bounds[0] + viewport.x;
            }
            else if (pos.x > bounds[2] - viewport.x)
            {
                //Debug.Log($"XMax {newPos.x}, {viewport.x}, {bounds[2]}");
                newPos.x = bounds[2] - viewport.x;
            }

            if (pos.y < bounds[1] + viewport.y)
            {
                //Debug.Log($"YMin {newPos.y}, {viewport.y}, {bounds[1]}");
                newPos.y = bounds[1] + viewport.y;
            }
            else if (pos.y > bounds[3] - viewport.y)
            {
                //Debug.Log($"YMax {newPos.y}, {viewport.y}, {bounds[3]}");
                newPos.y = bounds[3] - viewport.y;
            }

            newPos.z = -10;
            transform.position = newPos;

            if (2f * zoomLevel * Camera.main.aspect > bounds[2] - bounds[0])
            {
                maxZoomLevel = (bounds[2] - bounds[0]) / Camera.main.aspect / 2f;
            }
        }

        public void Translate(Vector2 position)
		{
			Position += position;
		}

		public void FocusAnt(Selectable ant) => _state?.FocusAnt(ant);
	}
}