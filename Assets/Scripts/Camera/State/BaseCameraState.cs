using Ant;
using UnityEngine;

namespace AntCamera
{
	public abstract class BaseCameraState : IState
	{
		public abstract string Name { get; }
		protected Camera _camera;
		protected CameraController _cameraController;
		protected Transform _transform;
		
		protected float _currentZoomLevel;
		protected float _zoomVelocity;

		public BaseCameraState(CameraController cameraController)
		{
			_camera = Camera.main;
			_cameraController = cameraController;
			_transform = _cameraController.GetComponent<Transform>();
		}

		public virtual void OnStateEnter()
		{
			_cameraController.zoomLevel = Mathf.Clamp(_cameraController.zoomLevel, _cameraController.minZoomLevel,
				_cameraController.maxZoomLevel);
			_currentZoomLevel = _cameraController.zoomLevel;
		}

		public virtual void OnStateExit()
		{ }

		public virtual void Tick(float deltaTime)
		{
			ZoomTick(deltaTime);
		}

		public virtual void FocusAnt(Selectable ant)
		{ }
		
		private void ZoomTick(float deltaTime)
		{
			float scroll = Input.mouseScrollDelta.y;
			// Snap scroll diff to -1, 0, or 1
			scroll = Mathf.Abs(scroll) > 0.1f ? (int) Mathf.Sign(-scroll) : 0;
			_cameraController.zoomLevel += scroll * _cameraController.zoomSpeed;
			_cameraController.zoomLevel = Mathf.Clamp(_cameraController.zoomLevel, _cameraController.minZoomLevel,
				_cameraController.maxZoomLevel);
			_currentZoomLevel = Mathf.SmoothDamp(_currentZoomLevel, _cameraController.zoomLevel, ref _zoomVelocity,
				0.1f, 1000, deltaTime);
			_camera.orthographicSize = _currentZoomLevel;
		}
	}
}