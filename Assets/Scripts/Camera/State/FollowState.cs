using Ant;
using UnityEngine;

namespace AntCamera
{
	public class FollowState : BaseCameraState
	{
        public override string Name => "Follow";
        
		private Selectable _ant;
		private Vector2 _desiredPosition;
		private Vector2 _velocity;

		public FollowState(CameraController cameraController, Selectable ant) : base(cameraController)
		{
			_ant = ant;
			_desiredPosition = _ant.Position;
		}

		public override void FocusAnt(Selectable ant)
		{
			if (ant == null)
			{
				_cameraController.State = new FreeState(_cameraController);
			}
			else
			{
				_cameraController.State = new FollowState(_cameraController, ant);
			}
		}

		public override void Tick(float deltaTime)
		{
			base.Tick(deltaTime);
			_desiredPosition = _ant.Position;
			_cameraController.Position = Vector2.SmoothDamp(_cameraController.Position, _desiredPosition, ref _velocity,
				0.1f, 1000, deltaTime);
		}
	}
}