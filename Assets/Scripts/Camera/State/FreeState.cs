using Ant;
using UnityEngine;

namespace AntCamera
{
	public class FreeState : BaseCameraState
	{
        public override string Name => "Free";
		private Vector2 _dragOrigin;
		private bool _dragging;

		public FreeState(CameraController cameraController) : base(cameraController)
		{ }

		public override void OnStateEnter()
		{
			base.OnStateEnter();
			_dragging = Input.GetMouseButton(1);
			if (_dragging)
			{
				_dragOrigin = _camera.ScreenToWorldPoint(Input.mousePosition);
			}
		}

		public override void FocusAnt(Selectable ant)
		{
			if (ant == null) return;

			_cameraController.State = new FollowState(_cameraController, ant);
		}

		public override void Tick(float deltaTime)
		{
			base.Tick(deltaTime);
			MouseDragTick();
		}

		private void MouseDragTick()
		{
			if (Input.GetMouseButtonDown(1) && !_dragging)
			{
				_dragOrigin = _camera.ScreenToWorldPoint(Input.mousePosition);
				_dragging = true;
				return;
			}
			else if (Input.GetMouseButtonUp(1))
			{
				_dragging = false;
			}

			if (_dragging)
			{
				Vector2 dragPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
				Vector2 distance = _dragOrigin - dragPosition;
				_cameraController.Translate(distance);
			}
		}

	}
}